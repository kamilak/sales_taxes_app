#!/usr/bin/env ruby

require_relative 'product'

class ProductParser
  def parse(input)
    if match = input.match('\A(\d), ([a-zA-Z ]+), ([0-9]*\.?[0-9]+)\z')
      quantity, name, price = match.captures
      
      imported = name.include?("imported")
      Product.new(quantity.to_f, name, price.to_f, imported)
    end
  end

  def parse_all(inputs)
    inputs.map! { |input| parse(input) }.select { |product| !product.nil? }
  end
end

class TaxCalculator
  def calculate_taxes(products, taxes, categories)
    products.map! { |product| apply_taxes(product, taxes, categories) }
  end

private
  def apply_taxes(product, taxes, categories)
    product_name = product.name.gsub("imported ", "")
    tax = taxes.fetch(
      categories.fetch(product_name, "all"), 0.0)
    
    tax += taxes.fetch("imported", 0.0)  if product.imported
       
    product.apply_tax(tax)
    product
  end
end

class InvoicePrinter
  def print_receipt(taxed_products) 
    if taxed_products.any?
      total, taxed_total = 0.0, 0.0
      
      taxed_products.each do |product|
        puts receipt(product)
        
        total       += product.quantity * product.price
        taxed_total += product.quantity * product.taxed_price
      end
  
      puts "Sales Taxes: %.2f" % (taxed_total - total)
      puts "Total: %.2f"       % taxed_total
    end
  end

private 
  def receipt(product)
     "%d, %s, %.2f" % [product.quantity, product.name, product.quantity * product.taxed_price]
  end
end

class SaleTaxesApp
  def collect_lines
   inputs = []
   
   loop do
      line = gets.chomp 
      inputs.push(line)
      break if line.empty?
    end 
    
    inputs
  end
    
  def main
    taxes = {"all" => 0.1, "imported" => 0.05, "books" => 0.0, "food" => 0.0, "medical" => 0.0}
    categories = {"book" => "books", "chocolate bar" => "food", "box of chocolates" => "food", "packet of headache pills" => "medical"}
        
    products = ProductParser.new.parse_all(collect_lines)
  
    taxed_products = TaxCalculator.new.calculate_taxes(products, taxes, categories)
    
    InvoicePrinter.new.print_receipt(taxed_products)
  end
end

SaleTaxesApp.new.main
