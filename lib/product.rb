class Product
  attr_accessor :quantity, :name, :price, :taxed_price, :imported
  
  def initialize(quantity, name, price, imported)
    @quantity, @name, @price, @taxed_price, @imported = quantity, name, price, price, imported
  end
  
  def apply_tax(tax)
    @taxed_price = @taxed_price * (1 + tax) 
  end
end

