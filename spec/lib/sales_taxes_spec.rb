require "spec_helper"
require "product"
require "sales_taxes"

describe ProductParser do
  describe 'for valid input' do
    it 'parses single input' do
      product = ProductParser.new.parse('1, book, 2.00') 
      
      expect(product.quantity).to eq 1
      expect(product.name).to     eq 'book'
      expect(product.price).to    eq 2.00
      expect(product.imported).to eq false
    end
    
    it 'parses multiple inputs' do
      products = ProductParser.new.parse_all(['1, book, 2.00', '1, chocolate, 2.00'])
      
      expect(products.length).to eq 2  
    end
  end
  
  describe 'for invalid input' do
    it 'returns null object' do
      product = ProductParser.new.parse('1.00s, book, 2.00')
      
      expect(product).to be_nil
    end
  end
  
  describe 'for some valid and invalid inputs' do
    it 'parses only valid' do
      products = ProductParser.new.parse_all(['1, book, 2.00', '1.00, chocolate, 2.00', '', '1, chocolate, 2.00'])
      
      expect(products.length).to eq 2
    end
  end
end

describe TaxCalculator do
  
  let(:taxes)      { { 'all' => 0.10, 'imported' => 0.05, 'books' => 0.00 } }
  let(:categories) { { 'book' => 'books', 'untaxed_product' => 'untaxed_category' } }
  
  describe 'for not categorised product' do
    
    let(:uncategorized_product)  { Product.new(1, 'cd', 1.00, false) }
    let(:uncategorized_products) { [uncategorized_product] }
    
    it 'applies default tax' do
      TaxCalculator.new.calculate_taxes(uncategorized_products, taxes, categories)
      
      expect(uncategorized_product.taxed_price).to eq 1.10
    end
  end
  
  describe 'for matching product, categories and taxes' do
    
    let(:exempt_product)    { Product.new(1, 'book', 1.00, false) }
    let(:exempt_products)   { [exempt_product] }
    
    it 'does not apply tax for exempt product' do
      TaxCalculator.new.calculate_taxes(exempt_products, taxes, categories)
      
      expect(exempt_product.taxed_price).to eq 1.00
    end
   
    let(:imported_product)  { Product.new(1, 'cd', 1.00, true) }
    let(:imported_products) { [imported_product] }
  
    it 'applies additional tax for imported product' do
      TaxCalculator.new.calculate_taxes(imported_products, taxes, categories)
      
      expect(imported_product.taxed_price).to eq 1.15
    end
  end
  
  describe 'for product in not taxed category' do
  
    let(:untaxed_product)   { Product.new(1, 'untaxed_product', 1.00, false) }
    let(:untaxed_products)  { [untaxed_product] }
  
    it 'applies no tax' do
      TaxCalculator.new.calculate_taxes(untaxed_products, taxes, categories)
      
      expect(untaxed_product.taxed_price).to eq 1.00
    end
  end
end

describe InvoicePrinter do
  
  let(:product)  { Product.new(1, 'book', 2.40, false) }
  let(:products) { [product] }
  
  describe 'for no products' do
    it 'does not print a receipt' do
      expect { InvoicePrinter.new.print_receipt([]) }.to_not output.to_stdout
    end
  end
  
  describe 'for valid products' do
    it 'prints receipt entry' do
      expect { InvoicePrinter.new.print_receipt(products) }.to output(/1, book, 2.40/).to_stdout
    end
    
    it 'prints receipt summary' do
      expect { InvoicePrinter.new.print_receipt(products) }.to output(/Sales Taxes: 0.00\nTotal: 2.40\n/).to_stdout
    end
  end
end
