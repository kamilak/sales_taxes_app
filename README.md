## Problem ##

Sales Taxes
Basic sales tax is applicable at a rate of 10% on all goods, except books, food, and medical products that are exempt. Import duty is an additional sales tax applicable on all imported goods at a rate of 5%, with no exemptions. When I purchase items I receive a receipt that lists the name of all the items and their price (including tax), finishing with the total cost of the items, and the total amounts of sales taxes paid. The rounding rules for sales tax are that for a tax rate of n%, a shelf price of p contains
(np/100 rounded up to the nearest 0.05) amount of sales tax. Write an application that prints out the receipt details for these shopping baskets.

##Solution##

Application consists of three parts, responsible for input parsing (ProductParser), 
appling taxes (TaxCalculator) and dispalying the results (InvoicePrinter).
Product class keeps details regarding product: quantity, name, price, taxed price and imported flag.
SalexTaxesApp is a client class which calles above mentioned components.

Program uses stdout and stdin to work with the user and one can make use of stdin and stdout redirections
to use script with files.

##How to run##

Type `ruby lib/sales_taxes.rb` then provide input lines, empty line closes input stream. 
Type `ruby lib/sales_taxes.rb < sample_inputs/input_1.txt` in order to process inputs defined in test file. 

Predefined test files in sample_input dir: input_1.txt, input_2.txt, input_3.txt.

Type `rspec spec/lib/sales_taxes_spec.rb --format documentation` to run tests.
